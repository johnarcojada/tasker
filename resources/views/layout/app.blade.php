<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
</head>
<body class="overflow-hidden">

    <div id="app">

        <div class="cursor">
            <div class="cursor__inner cursor__inner--circle"></div>
            <div class="cursor__inner cursor__inner--dot"></div>
        </div>
        <main>
            <section class="toDo_slidenav">
                <button class="close btn-icon btn-ripple" data-hover>
                    <i class="material-icons">close</i>
                </button>
                <div class="wrapper scroller">
                    <p class="title">To-Do</p>
                    <div class="head editing">
                        <div class="d-flex align-items-center">
                            <vue-color></vue-color>
                            <div class="head_title">
                                <input id="toDo_slidenav_title" class="p-2" type="text" value="Greet Everyone with a smile :)">
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <vue-texteditor></vue-texteditor>
                    </div>
                </div>
            </section>
            <div class="toDo_slidenav_overlay"></div>
            <section class="sidebar">

                <div class="logo py-4">
                    <img src="{{ asset('assets/images/logo.png') }}" alt="">
                </div>

                <div class="task_list_wrapper scroller">
                    <div class="task_list">

                        @for($i = 1; $i <= 2; $i++)
                        <div class="task">

                            <div class="head d-flex justify-content-between align-items-center">
                                <div class="mark_as_complete">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="chkbx_task{{ $i }}">
                                        <label class="custom-control-label" for="chkbx_task{{ $i }}" data-hover></label>
                                    </div>
                                </div>
                                <div class="title">
                                    Task Title {{ $i }}
                                </div>
                                <div class="action">
                                    <button class="btn-icon btn-ripple" id="task_more_menu_{{ $i }}" data-hover>
                                        <i class="material-icons">more_vert</i>
                                    </button>
                                    <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                        for="task_more_menu_{{ $i }}">
                                        <li class="mdl-menu__item" data-hover>
                                            <div class="d-flex align-items-center">
                                                <i class="far fa-trash-alt mr-2"></i>
                                                Delete Task
                                            </div>
                                        </li>
                                    </ul>

                                </div>
                            </div>

                            <div class="body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Accusamus atque cumque doloribus eius eum iure, iusto laborum
                                recusandae, repellendus saepe ut vel velit voluptatum. Hic
                                illum laboriosam magnam modi nulla.
                            </div>

                        </div>
                        @endfor

                    </div>
                </div>

                <div class="add_task">
                    <button class="btn-ripple" data-hover>
                        <i class="fas fa-plus-circle mr-2"></i>
                        add new task
                    </button>
                </div>

            </section>
            <section class="main_container">
                <div class="main_wrapper scroller">
                    <div class="main_content">

                        <div class="head">
                            <div class="title">
                                Make Something Valuable
                            </div>
                            <div class="description">
                                Task Description are detailed statement of work that accompanies a task order in construction contracts.
                                Task Descriptions are the statements of scope for each of the project activities.  They are written in the format of “action – completion point.
                            </div>
                        </div>
                        <div class="body">
                            <div class="title">To-Dos :</div>
                            <div class="toDo_list">

                                <div class="toDo_list_wrapper mb-5">

                                    @for($i = 1; $i <= 3; $i++)
                                    <div class="toDo_list_item">
                                        <div class="d-flex align-items-center">
                                            <div class="toDo_color mr-4" style="background: #179779;"></div>
                                            <div class="toDo_title">
                                                Greet Everyone with a smile :)
                                            </div>
                                            <div class="ml-auto toDo_action">
                                                <div class="d-flex align-items-center">
                                                    <button class="btn-icon btn-ripple mr-2" data-hover>
                                                        <i class="far fa-edit"></i>
                                                    </button>
                                                    <button class="btn-icon btn-ripple" data-hover>
                                                        <i class="far fa-trash-alt"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endfor

                                </div>

                                <div class="add_to_do">
                                    <div class="d-flex align-items-center">
                                        <div class="icon mr-3" data-hover>
                                            <span></span>
                                            <span></span>
                                        </div>
                                        <div class="text">Add New To-Do</div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </main>

    </div>

    <script src="{{ asset('assets/js/app.js') }}"></script>
    <script>
        (function () {
            $('body').on('click', '.add_to_do .icon', function () {
                $('.toDo_slidenav').toggleClass('active')
                $('.toDo_slidenav_overlay').toggleClass('active')
            })
            $('body').on('click', '.toDo_slidenav .close', function () {
                $('.toDo_slidenav').toggleClass('active')
                $('.toDo_slidenav_overlay').toggleClass('active')
            })
        })()
    </script>
</body>
</html>

require('perfect-scrollbar/dist/perfect-scrollbar')

import PerfectScrollbar from 'perfect-scrollbar'

$('.scroller').each(function () {
    const ps = new PerfectScrollbar($(this)[0])
})
